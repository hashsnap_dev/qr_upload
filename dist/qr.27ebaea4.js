// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles

// eslint-disable-next-line no-global-assign
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  for (var i = 0; i < entry.length; i++) {
    newRequire(entry[i]);
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  return newRequire;
})({"../../AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/bundle-url.js":[function(require,module,exports) {
var bundleURL = null;

function getBundleURLCached() {
  if (!bundleURL) {
    bundleURL = getBundleURL();
  }

  return bundleURL;
}

function getBundleURL() {
  // Attempt to find the URL of the current script and use that as the base URL
  try {
    throw new Error();
  } catch (err) {
    var matches = ('' + err.stack).match(/(https?|file|ftp):\/\/[^)\n]+/g);

    if (matches) {
      return getBaseURL(matches[0]);
    }
  }

  return '/';
}

function getBaseURL(url) {
  return ('' + url).replace(/^((?:https?|file|ftp):\/\/.+)\/[^/]+$/, '$1') + '/';
}

exports.getBundleURL = getBundleURLCached;
exports.getBaseURL = getBaseURL;
},{}],"../../AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/css-loader.js":[function(require,module,exports) {
var bundle = require('./bundle-url');

function updateLink(link) {
  var newLink = link.cloneNode();

  newLink.onload = function () {
    link.remove();
  };

  newLink.href = link.href.split('?')[0] + '?' + Date.now();
  link.parentNode.insertBefore(newLink, link.nextSibling);
}

var cssTimeout = null;

function reloadCSS() {
  if (cssTimeout) {
    return;
  }

  cssTimeout = setTimeout(function () {
    var links = document.querySelectorAll('link[rel="stylesheet"]');

    for (var i = 0; i < links.length; i++) {
      if (bundle.getBaseURL(links[i].href) === bundle.getBundleURL()) {
        updateLink(links[i]);
      }
    }

    cssTimeout = null;
  }, 50);
}

module.exports = reloadCSS;
},{"./bundle-url":"../../AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/bundle-url.js"}],"qr.scss":[function(require,module,exports) {
var reloadCSS = require('_css_loader');

module.hot.dispose(reloadCSS);
module.hot.accept(reloadCSS);
},{"./main.png":[["main.369f5891.png","main.png"],"main.png"],"./photo.png":[["photo.77c32ced.png","photo.png"],"photo.png"],"_css_loader":"../../AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/css-loader.js"}],"qr.js":[function(require,module,exports) {
"use strict";

require("./qr.scss");

var fixedCover = document.querySelector('.fixed_cover');
var mainCover = document.getElementById('main_cover');
var contentsCover = document.getElementById('contents_container');
var input = document.getElementById('input_file');
var canvas = document.getElementById('main_canvas');
var temCover = document.querySelector('.tem_cover');
var ctx = canvas.getContext('2d');
var sendButton = document.getElementById('button_send');
var rightButton = document.querySelector('.rotate_right');
var invisibleCanvas = document.createElement('canvas');
var invisibleCtx = invisibleCanvas.getContext('2d');
var PROJECT_UID = 'b64e0056-4e07-470e-88cd-4a1c7cd0306b';
var REQUEST_URL = "https://api.hashsnap.net/posts/upload/".concat(PROJECT_UID);
var IMAGE_TYPE = 'image/png';
var IMAGE_NAME = 'test.png';
invisibleCanvas.width = 1000;
invisibleCanvas.height = invisibleCanvas.width;
var imageFile = '';
var HTMLCanvasPrototype = HTMLCanvasElement.prototype; //  Object.prototype은 객체의 속성이 있냐없냐에 따라 달라져서 직접호출하지말라고 하는듯하다.

HTMLCanvasPrototype.renderImage = function (blob) {
  //  change이벤트가 일어나면 욜로온다
  imageFile = new Image();

  imageFile.onload = function () {
    drawVisibleCanvas(ctx, imageFile);
    mainCover.style.display = 'none';
    contentsCover.style.display = "block";
  };

  imageFile.src = URL.createObjectURL(blob);
};

input.addEventListener('change', function () {
  return canvas.renderImage(input.files[0]);
}); //  신기방기

mainCover.addEventListener('click', function () {
  return input.click();
});
rightButton.addEventListener('click', function () {
  return imageRotate(90);
});
sendButton.addEventListener('click', function () {
  // temCover.style.opcity = 0.5;
  // temCover.style['z-index'] = 2;
  fixedCover.style.display = 'inline';
  uploadCanvas(canvas);
});

var imageRotate = function invisibleCanvasRotate(num) {
  //  rotate의 특징은 기준점을 바꾸는것
  //  (num, ctx, invisibleCtx, imageFile) 의존성 낮추기 매개변수
  var x = ctx.canvas.width / 2;
  var y = ctx.canvas.height / 2;
  var RADIAN = Math.PI / 180;
  var angleInRadians = num * RADIAN;
  invisibleCanvas.width = ctx.canvas.width;
  invisibleCanvas.height = ctx.canvas.height;
  invisibleCtx.translate(x, y);
  invisibleCtx.rotate(angleInRadians);
  invisibleCtx.translate(-x, -y);
  drawInvisibleCanvas(ctx, invisibleCtx, imageFile);
};

var resizing = function visibleCanvasSetSize(e) {
  var sizing = e.target.value;
  canvas.width = sizing;
  canvas.height = sizing;
  drawVisibleCanvas(ctx, imageFile);
};

var drawInvisibleCanvas = function invisibleCanvasDrawCover(ctx, invisibleCtx, img) {
  //invisible ctx
  var x = 0,
      y = 0,
      w = 0,
      h = 0;

  if (arguments.length === 3) {
    w = ctx.canvas.width;
    h = ctx.canvas.height;
  }

  var iw = img.width,
      ih = img.height,
      r = Math.min(w / iw, h / ih),
      nw = iw * r,
      nh = ih * r,
      cx,
      cy,
      cw,
      ch,
      ar = 1;
  if (nw < w) ar = w / nw;
  if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;
  nw *= ar;
  nh *= ar;
  cw = iw / (nw / w);
  ch = ih / (nh / h);
  cx = iw - cw;
  cy = ih - ch;
  if (cx < 0) cx = 0;
  if (cy < 0) cy = 0;
  if (cw > iw) cw = iw;
  if (ch > ih) ch = ih;
  invisibleCtx.drawImage(ctx.canvas, 0, 0);
  drawVisibleCanvas(ctx, invisibleCtx.canvas);
};

var drawVisibleCanvas = function visibleCanvasDrawCover(ctx, img, x, y, w, h, offsetX, offsetY) {
  // ctx
  if (arguments.length === 2) {
    x = y = 0;
    w = ctx.canvas.width;
    h = ctx.canvas.height;
  }

  offsetX = typeof offsetX === "number" ? offsetX : 0.5;
  offsetY = typeof offsetY === "number" ? offsetY : 0.5;
  var iw = img.width,
      ih = img.height,
      r = Math.min(w / iw, h / ih),
      nw = iw * r,
      nh = ih * r,
      cx,
      cy,
      cw,
      ch,
      ar = 1;
  if (nw < w) ar = w / nw;
  if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh;
  nw *= ar;
  nh *= ar;
  cw = iw / (nw / w);
  ch = ih / (nh / h);
  cx = (iw - cw) * offsetX;
  cy = (ih - ch) * offsetY;
  if (cx < 0) cx = 0;
  if (cy < 0) cy = 0;
  if (cw > iw) cw = iw;
  if (ch > ih) ch = ih;
  ctx.drawImage(img, cx, cy, cw, ch, x, y, w, h);
};

var progressBar = function progressBar(v) {
  console.log(v);
};

var canvasToBlob = function canvasToBlob(canvas, _ref) {
  var type = _ref.type,
      quality = _ref.quality;
  return new Promise(function (resolve) {
    return canvas.toBlob(resolve, type, quality);
  });
};

var blobToFile = function blobToFile(blob, _ref2) {
  var name = _ref2.name,
      type = _ref2.type;
  return new Promise(function (resolve) {
    return resolve(new File([blob], name, {
      type: type,
      lastModified: Date.now()
    }));
  });
};

var fileToFormData = function fileToFormData(params) {
  return new Promise(function (resolve) {
    var fd = new FormData();

    for (var name in params) {
      fd.append(name, params[name]);
    }

    resolve(fd);
  });
};

var sendFormData = function sendFormData(fd, src) {
  var f = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};
  return new Promise(function (resolve) {
    var xhr = new XMLHttpRequest();

    xhr.onload = function (ev) {
      return resolve(ev);
    };

    xhr.upload.onprogress = function (_ref3) {
      var loaded = _ref3.loaded,
          total = _ref3.total;
      return f(loaded / total);
    };

    xhr.open('POST', src, true);
    xhr.send(fd);
  });
};

var uploadCanvas = function uploadCanvas(canvas) {
  return Promise.resolve(canvas).then(function (canvas) {
    return canvasToBlob(canvas, {
      type: IMAGE_TYPE,
      quality: 1
    });
  }).then(function (blob) {
    return blobToFile(blob, {
      name: IMAGE_NAME,
      type: IMAGE_TYPE
    });
  }).then(function (file) {
    return fileToFormData({
      image: file,
      resolver: 'instagram'
    });
  }).then(function (fd) {
    return sendFormData(fd, REQUEST_URL, progressBar);
  }).then(function (data) {
    console.log(data.target.response); // temCover.style['z-index'] = -1;
    // temCover.style.opcity = 0;

    fixedCover.style.display = 'none';
    mainCover.style.display = 'inline';
    contentsCover.style.display = "none";
  });
};
},{"./qr.scss":"qr.scss"}],"../../AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "50909" + '/');

  ws.onmessage = function (event) {
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      console.clear();
      data.assets.forEach(function (asset) {
        hmrApply(global.parcelRequire, asset);
      });
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          hmrAccept(global.parcelRequire, asset.id);
        }
      });
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAccept(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAccept(bundle.parent, id);
  }

  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAccept(global.parcelRequire, id);
  });
}
},{}]},{},["../../AppData/Roaming/npm/node_modules/parcel-bundler/src/builtins/hmr-runtime.js","qr.js"], null)
//# sourceMappingURL=/qr.27ebaea4.map