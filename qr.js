  
  import './qr.scss';

  const fixedCover = document.querySelector('.fixed_cover');
  const mainCover = document.getElementById('main_cover');
  const contentsCover = document.getElementById('contents_container');
  const input = document.getElementById('input_file');
  const canvas = document.getElementById('main_canvas');
  const temCover = document.querySelector('.tem_cover');
  const ctx = canvas.getContext('2d');
  const sendButton = document.getElementById('button_send');
  const rightButton = document.querySelector('.rotate_right');
  const invisibleCanvas = document.createElement('canvas');
  const invisibleCtx = invisibleCanvas.getContext('2d');
  const PROJECT_UID = 'b64e0056-4e07-470e-88cd-4a1c7cd0306b';
  const REQUEST_URL = `https://api.hashsnap.net/posts/upload/${ PROJECT_UID }`;
  const IMAGE_TYPE = 'image/png';
  const IMAGE_NAME = 'test.png';

  invisibleCanvas.width = 1000;
  invisibleCanvas.height = invisibleCanvas.width;
  let imageFile = '';
  const HTMLCanvasPrototype = HTMLCanvasElement.prototype;  //  Object.prototype은 객체의 속성이 있냐없냐에 따라 달라져서 직접호출하지말라고 하는듯하다.

  HTMLCanvasPrototype.renderImage = (blob) => { //  change이벤트가 일어나면 욜로온다
    imageFile = new Image();
    
    imageFile.onload = () => {
      drawVisibleCanvas(ctx, imageFile);
      mainCover.style.display = 'none';
      contentsCover.style.display = "block";
    }
    imageFile.src = URL.createObjectURL(blob);
  }

  input.addEventListener('change', () => canvas.renderImage(input.files[0]));  //  신기방기
  mainCover.addEventListener('click', () => input.click());
  rightButton.addEventListener('click', () => imageRotate(90));
  sendButton.addEventListener('click', () => {
    // temCover.style.opcity = 0.5;
    // temCover.style['z-index'] = 2;
    fixedCover.style.display = 'inline';
    uploadCanvas(canvas);
  });

  const imageRotate = function invisibleCanvasRotate (num) {  //  rotate의 특징은 기준점을 바꾸는것
    //  (num, ctx, invisibleCtx, imageFile) 의존성 낮추기 매개변수

    const x = ctx.canvas.width / 2;
    const y = ctx.canvas.height / 2;
    const RADIAN = Math.PI/180;
    const angleInRadians = num * RADIAN;
    invisibleCanvas.width = ctx.canvas.width;
    invisibleCanvas.height = ctx.canvas.height;
    invisibleCtx.translate(x, y);
    invisibleCtx.rotate(angleInRadians);
    invisibleCtx.translate(-x, -y);
    drawInvisibleCanvas(ctx, invisibleCtx, imageFile);
  }

  const resizing = function visibleCanvasSetSize (e) {
    const sizing = e.target.value;
    canvas.width = sizing;
    canvas.height = sizing;
    drawVisibleCanvas(ctx, imageFile);
  }

  const drawInvisibleCanvas = function invisibleCanvasDrawCover (ctx, invisibleCtx, img) {
    //invisible ctx

    let x = 0,
        y = 0,
        w = 0,
        h = 0;
    if (arguments.length === 3) {
        w = ctx.canvas.width;
        h = ctx.canvas.height;
    }

    let iw = img.width, 
        ih = img.height,
        r = Math.min(w / iw, h / ih),
        nw = iw * r,   
        nh = ih * r,   
        cx, cy, cw, ch, ar = 1;

    if (nw < w) ar = w / nw;                             
    if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh; 

    nw *= ar;
    nh *= ar;
    cw = iw / (nw / w);
    ch = ih / (nh / h);
    cx = (iw - cw);
    cy = (ih - ch);

    if (cx < 0) cx = 0;
    if (cy < 0) cy = 0;
    if (cw > iw) cw = iw;
    if (ch > ih) ch = ih;

    invisibleCtx.drawImage(ctx.canvas, 0, 0);
    drawVisibleCanvas(ctx, invisibleCtx.canvas);
  }

  const drawVisibleCanvas = function visibleCanvasDrawCover (ctx, img, x, y, w, h, offsetX, offsetY) {
    // ctx

    if (arguments.length === 2) {
        x = y = 0;
        w = ctx.canvas.width;
        h = ctx.canvas.height;
    }

    offsetX = typeof offsetX === "number" ? offsetX : 0.5;
    offsetY = typeof offsetY === "number" ? offsetY : 0.5;

    let iw = img.width, 
        ih = img.height,
        r = Math.min(w / iw, h / ih),
        nw = iw * r,
        nh = ih * r,
        cx, cy, cw, ch, ar = 1;

    if (nw < w) ar = w / nw;                             
    if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh; 

    nw *= ar;
    nh *= ar;
    cw = iw / (nw / w);
    ch = ih / (nh / h);
    cx = (iw - cw) * offsetX;
    cy = (ih - ch) * offsetY;

    if (cx < 0) cx = 0;
    if (cy < 0) cy = 0;
    if (cw > iw) cw = iw;
    if (ch > ih) ch = ih;

    ctx.drawImage(img, cx, cy, cw, ch, x, y, w, h);
  }

const progressBar = (v) => {
  console.log(v);
};

const canvasToBlob = (canvas, { type, quality }) => 
  new Promise(
    resolve => canvas.toBlob(resolve, type, quality)
  );

const blobToFile = (blob, { name, type }) => 
  new Promise(
    resolve => resolve(new File([blob], name, { type, lastModified: Date.now() }))
  );

const fileToFormData = (params) =>
  new Promise(resolve => {
    const fd = new FormData;
    for (const name in params) fd.append(name, params[name]); 
    resolve(fd);
  });

const sendFormData = (fd, src, f = () => {}) => {
  return new Promise(resolve => {
    const xhr = new XMLHttpRequest;
    xhr.onload = (ev) => resolve(ev);
    xhr.upload.onprogress = ({ loaded, total}) => f(loaded / total);
    xhr.open('POST', src, true);
    xhr.send(fd);
  });
};

const uploadCanvas = (canvas) => {
  return Promise.resolve(canvas)
    .then((canvas) => canvasToBlob(canvas, { type: IMAGE_TYPE, quality: 1 }))
    .then((blob) => blobToFile(blob, { name: IMAGE_NAME, type: IMAGE_TYPE }))
    .then((file) => fileToFormData({ image: file, resolver: 'instagram' }))
    .then((fd) => sendFormData(fd, REQUEST_URL, progressBar))
    .then(data => {
      console.log(data.target.response)
      // temCover.style['z-index'] = -1;
      // temCover.style.opcity = 0;
      fixedCover.style.display = 'none';
      mainCover.style.display = 'inline';
      contentsCover.style.display = "none";
    });
};
